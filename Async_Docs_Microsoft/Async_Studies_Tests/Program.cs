﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Async_Studies_Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    class Samples // FROM Microsoft documentation https://docs.microsoft.com/en-us/dotnet/csharp/async
    {
        //private readonly HttpClient _httpClient = new HttpClient();
        //downloadButton.Clicked += async(o, e) =>
        //{
        //// This line will yield control to the UI as the request
        //// from the web service is happening.
        ////
        //// The UI thread is now free to perform other work.
        //var stringData = await _httpClient.GetStringAsync(URL);
        //    DoSomethingWithData(stringData);
        //};  


        //private DamageResult CalculateDamageDone()
        //{
        //    // Code omitted:
        //    //
        //    // Does an expensive calculation and returns
        //    // the result of that calculation.
        //}
        //calculateButton.Clicked += async(o, e) =>
        //{
        //// This line will yield control to the UI while CalculateDamageDone()
        //// performs its work. The UI thread is free to perform other work.
        //var damageResult = await Task.Run(() => CalculateDamageDone());
        //    DisplayDamage(damageResult);
        //};


        //private readonly HttpClient _httpClient = new HttpClient();
        //[HttpGet, Route("DotNetCount")]
        //public async Task<int> GetDotNetCount()
        //{
        //    // Suspends GetDotNetCount() to allow the caller (the web server)
        //    // to accept another request, rather than blocking on this one.
        //    var html = await _httpClient.GetStringAsync("https://dotnetfoundation.org");
        //    return Regex.Matches(html, @"\.NET").Count;
        //}


        //private readonly HttpClient _httpClient = new HttpClient();
        //private async void OnSeeTheDotNetsButtonClick(object sender, RoutedEventArgs e)
        //{
        //    // Capture the task handle here so we can await the background task later.
        //    var getDotNetFoundationHtmlTask = _httpClient.GetStringAsync("https://dotnetfoundation.org");

        //    // Any other work on the UI thread can be done here, such as enabling a Progress Bar.
        //    // This is important to do here, before the "await" call, so that the user
        //    // sees the progress bar before execution of this method is yielded.
        //    NetworkProgressBar.IsEnabled = true;
        //    NetworkProgressBar.Visibility = Visibility.Visible;

        //    // The await operator suspends OnSeeTheDotNetsButtonClick(), returning control to its caller.
        //    // This is what allows the app to be responsive and not block the UI thread.
        //    var html = await getDotNetFoundationHtmlTask;
        //    int count = Regex.Matches(html, @"\.NET").Count;

        //    DotNetCountLabel.Text = $"Number of .NETs on dotnetfoundation.org: {count}";

        //    NetworkProgressBar.IsEnabled = false;
        //    NetworkProgressBar.Visibility = Visibility.Collapsed;
        //}


        //public async Task<User> GetUserAsync(int userId)
        //{
        //    // Code omitted:
        //    //
        //    // Given a user Id {userId}, retrieves a User object corresponding
        //    // to the entry in the database with {userId} as its Id.
        //}
        //public static async Task<IEnumerable<User>> GetUsersAsync(IEnumerable<int> userIds)
        //{
        //    var getUserTasks = new List<Task<User>>();
        //    foreach (int userId in userIds)
        //    {
        //        getUserTasks.Add(GetUserAsync(userId));
        //    }

        //    return await Task.WhenAll(getUserTasks);
        //}



        //public async Task<User> GetUserAsync(int userId)
        //{
        //    // Code omitted:
        //    //
        //    // Given a user Id {userId}, retrieves a User object corresponding
        //    // to the entry in the database with {userId} as its Id.
        //}
        //public static async Task<User[]> GetUsersAsync(IEnumerable<int> userIds)
        //{
        //    var getUserTasks = userIds.Select(id => GetUserAsync(id));
        //    return await Task.WhenAll(getUserTasks);
        //}
    }


    public class AwaitOperator //Documentation from https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/await
    {
        public static async Task Main()
        {
            Task<int> downloading = DownloadDocsMainPageAsync();
            Console.WriteLine($"{nameof(Main)}: Launched downloading.");

            int bytesLoaded = await downloading;
            Console.WriteLine($"{nameof(Main)}: Downloaded {bytesLoaded} bytes.");
        }

        private static async Task<int> DownloadDocsMainPageAsync()
        {
            Console.WriteLine($"{nameof(DownloadDocsMainPageAsync)}: About to start downloading.");

            var client = new HttpClient();
            byte[] content = await client.GetByteArrayAsync("https://docs.microsoft.com/en-us/");

            Console.WriteLine($"{nameof(DownloadDocsMainPageAsync)}: Finished downloading.");
            return content.Length;
        }
    }
}
